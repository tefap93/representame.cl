import { Component, OnInit } from '@angular/core';
import {OpenDialogInicioSesionComponent} from "../open-dialog-inicio-sesion/open-dialog-inicio-sesion.component";
import {OpenDialogRegistroComponent} from "../open-dialog-registro/open-dialog-registro.component";
import {MatDialog} from "@angular/material";

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  constructor(public dialog : MatDialog) { }

  ngOnInit() {
  }
  dialogInicioSesion(): void {
    this.dialog.open(OpenDialogInicioSesionComponent);
  }
  dialogRegistro(): void {
    this.dialog.open(OpenDialogRegistroComponent);
  }
}
