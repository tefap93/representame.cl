
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {UsuarioComponent} from './usuario/usuario.component';
import {LandingComponent} from './landing/landing.component';

const routes: Routes = [
    { path: 'usuario', component: UsuarioComponent },
    {path : '', component : LandingComponent}
];
@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})
export class AppRoutingModule { }