import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenDialogInicioSesionComponent } from './open-dialog-inicio-sesion.component';

describe('OpenDialogInicioSesionComponent', () => {
  let component: OpenDialogInicioSesionComponent;
  let fixture: ComponentFixture<OpenDialogInicioSesionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenDialogInicioSesionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenDialogInicioSesionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
