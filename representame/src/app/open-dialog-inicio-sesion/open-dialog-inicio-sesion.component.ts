
import {Component, Inject, Injectable} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA, MatDialog} from '@angular/material';

@Component({
  selector: 'dialog-inicio-sesion',
  templateUrl: './open-dialog-inicio-sesion.component.html',
  styleUrls: ['./open-dialog-inicio-sesion.component.css']
})
export class OpenDialogInicioSesionComponent{

  constructor(private dialogRef: MatDialogRef<OpenDialogInicioSesionComponent>, @Inject(MAT_DIALOG_DATA) public data : any) {

  }
}