import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import {UsuarioComponent} from "./usuario/usuario.component";
import {LoginComponent} from "./login/login.component";
import { LandingComponent } from './landing/landing.component';
import {AppRoutingModule} from "./app.routing.module";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule } from '@angular/material/form-field';
import {MatInputModule } from '@angular/material';
import { OpenDialogInicioSesionComponent } from './open-dialog-inicio-sesion/open-dialog-inicio-sesion.component';
import { OpenDialogRegistroComponent } from './open-dialog-registro/open-dialog-registro.component';
import { RegistroComponent } from './registro/registro.component';
import {MatTabsModule} from '@angular/material/tabs';

@NgModule({
  declarations: [
    AppComponent,
    UsuarioComponent,
    LoginComponent,
    LandingComponent,
    OpenDialogInicioSesionComponent,
    OpenDialogRegistroComponent,
    RegistroComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatCheckboxModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatTabsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    OpenDialogInicioSesionComponent,
    OpenDialogRegistroComponent,
    ]

})

export class AppModule {

}