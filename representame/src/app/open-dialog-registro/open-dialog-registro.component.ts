import {Component, Inject, Injectable} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA, MatDialog} from '@angular/material';

@Component({
  selector: 'dialog-registro',
  templateUrl: './open-dialog-registro.component.html',
  styleUrls: ['./open-dialog-registro.component.css']
})
export class OpenDialogRegistroComponent{

  constructor(private dialogRef: MatDialogRef<OpenDialogRegistroComponent>, @Inject(MAT_DIALOG_DATA) public data : any) {
  }
}