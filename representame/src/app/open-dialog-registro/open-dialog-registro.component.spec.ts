import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenDialogRegistroComponent } from './open-dialog-registro.component';

describe('OpenDialogRegistroComponent', () => {
  let component: OpenDialogRegistroComponent;
  let fixture: ComponentFixture<OpenDialogRegistroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenDialogRegistroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenDialogRegistroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
