# DETALLES TAREA #
* Propuesta de integración entre landing y registro de app representame.cl 
* Diseño: Estefany Pacheco

##### Lenguajes / Métodos utilizados #
* HTML5
* Angular CLI
* Angular 5
* SCSS
* Flexbox 

#####  ¿Por qué? #
* HTML5. Para creación de elementos relativos al look & feel del landing page.
* Angular CLI. Para trabajar con Angular desde la linea de comandos.
* Angular 5. Es el lenguaje en que se encuentra desarrollada la aplicación existente, mantenerlo en el landing facilita la integración y mantiene limpieza y orden en el código.
* SCSS. Preprocesador de CSS, garantiza simplicidad y limpieza de código evitando la repetición innecesaria de clases y atributos y facilitando la lectura del código en si.
* Flexbox. La aplicación trabaja con flexbox, una vez más se aplica este método buscando facilitar la integración y evitar colisiones entre metodos aplicados en hojas de estilos, este además es garante de que el sitio sea responsive y completamente funcional en equipos móviles 

#####  Comentarios Adicionales #
* Para probar el inicio de sesión utiliza el mail "admin" y la contraseña "admin"
* La página de inicio de sesión es una muestra con una captura de pantalla de la aplicación actual 

#####  Contacto #
* Estefany Pacheco
* estefanyisabel93@gmail.com
* +56 9 3296 7727